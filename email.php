<?php 
# CONFIG
########################################
# CSS_TYPE - define the type of the css
# (default) - inline css; (regular) - no inline css; this will be used by deployer; eg. define('CSS_TYPE', 'default,regular');
# default value - 'default'
#define('CSS_TYPE', 'default,regular');

# PLATFORMS - define the platform we should use for this template
# the available palforms are:
# (default) - regular html tags; 
# (mailchimp) - mailchimp tags;
# (campaignmonitor) - campaignmonitor tags; 
# (pardot) - pardot tags; 
# (autopilot) - autopilot tags; 
# e.g define('PLATFORMS', 'html,mailchimp,campaignmonitor,pardot,autopilot');
# default value - 'default'
define('PLATFORMS', 'html');

# Whethear or not to save html, mailchimp and campaignmonitor as html files on refresh
# default value - false
define('SAVE_HTML', true);

# RETURN_TEMPLATE - which templates to returns in the outpot
# (default) - default-html, default-mailchimp, default-camaignmonitor  or regular-html, regular-mailchimp, regular-campaignmonitor
# default value - 'default-html'
# define('RETURN_TEMPLATE', 'default-html');
########################################

# The Funcs
include_once('lib/email-parser.php');
ob_start("parse_email_template");

# Path to your images
$p = 'images';
$title = PLATFORMS === 'mailchimp' ? '*|MC:SUBJECT|*' : 'Email Template';
$mcpreview = '';
if (PLATFORMS === 'mailchimp') {
    $mcpreview = '<!--*|IF:MC_PREVIEW_TEXT|*-->
		<!--[if !gte mso 9]><!-->
		<span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">*|MC_PREVIEW_TEXT|*</span>
		<!--<![endif]-->
	<!--*|END:IF|*-->
';} 

# Testing
# echo parse_email_template( file_get_contents('email-test.html') );
# exit;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
   	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet"/>
    <!--<![endif]-->
	<title><?php echo $title ?></title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	<style type="text/css">
		/* Only classes accepted */
		.background-body { background:#ffffff; }
		.p { padding: 0 !important; margin: 0 !important; }
		.body { padding: 0 !important; margin: 0 !important; display: block !important; min-width: 100% !important; width: 100% !important; background: .background-body; -webkit-text-size-adjust: none; }

		.m-td { font-size: 0pt; line-height: 0pt; text-align: left; }
		.fluid-img { font-size: 0pt; line-height: 0pt; text-align: left; }
		.content-spacing { font-size: 0pt; line-height: 0pt; text-align: left; }
		.spacer { font-size: 0pt; line-height: 0pt; text-align: center; width: 100%; min-width: 100%; }
		.border { font-size: 0pt; line-height: 0pt; text-align: center; width: 100%; min-width: 100%; }
		.column { font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal; Margin: 0; }
		.column-top { font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal; vertical-align: top; Margin: 0; }
		.column-bottom { font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal; vertical-align: bottom; Margin: 0; }
		.column-dir { font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal; direction: ltr; Margin: 0; }
		.column-dir-top { font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal; direction: ltr; vertical-align: top;  Margin: 0; }
		.column-dir-bottom { font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal; direction: ltr; vertical-align: bottom;  Margin: 0; }
		.td { width: 600px; min-width: 600px; font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal; Margin: 0; }

		.img { font-size: 0pt; line-height: 0pt; text-align: left; }
		.img-center { font-size: 0pt; line-height: 0pt; text-align: center; }
		.img-right { font-size: 0pt; line-height: 0pt; text-align: right; }

		.white { color: #ffffff; }
		.a-left { text-align: left; }
		.a-center { text-align: center; }
		.a-right { text-align: right; }

		.sup { font-size: 0.6em; line-height: 1; }

		.link { color: #ff0000; text-decoration: none; }
		.link-u { color: #ff0000; text-decoration: underline; }


		.text { color: #000000; font-family: 'Poppins', Arial, sans-serif; font-size: 12px; line-height: 16px; text-align: left; min-width: auto !important; }
		
		.h2 { color: #000000; font-family: 'Poppins', Arial, sans-serif; font-size: 20px; line-height: 24px; text-align: left; font-weight: bold; }
	</style>

	<style type="text/css" media="linked">
		/* Linked Styles */
		body { .body }
		a { .link }
		p { .p } 
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }

		<?php if (PLATFORMS === 'mailchimp') {
		echo ".cke_editable,
		.cke_editable a,
		.cke_editable span,
		.cke_editable a span { color: #000001 !important; }
		.tpl-content { padding: 0 !important; }";	
		}?>
		
		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			u + .body .gwfw { width:100% !important; width:100vw !important; }

			.m-shell { width: 100% !important; min-width: 100% !important; }
			
			.m-center { text-align: center !important; }
			
			.center { margin: 0 auto !important; }
			
			.td { width: 100% !important; min-width: 100% !important; }

			.m-br-15 { height: 15px !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column { float: left !important; width: 100% !important; display: block !important; }

			.content-spacing { width: 15px !important; }
		}
	</style>
</head>
<body class="body">
<?php echo $mcpreview ?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor=".background-body" class="gwfw">
		<tr>
			<td align="center" valign="top">
				<table width="600" border="0" cellspacing="0" cellpadding="0" class="m-shell">
					<tr>
						<td class="td">
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
<?php ob_end_flush(); ?>