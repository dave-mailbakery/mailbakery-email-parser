<?php
/**
*** ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
*** TAGS                Default              MailChimp                                      CampaignMonitor                 Pardot                                                                  Autopilot
*** ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
*** <multilne>          <div>                <div mc:edit="text_%s">                        <multiline>                     <div pardot-region="">                                                  <div mc:edit="text_%s">
*** <singleline>        <div>                <div mc:edit="title_%s">                       <singleline>                    <div>                                                                   <div>
*** <image src="" />    <img />              <img src="" mc:edit="image_%s" />              <img src="" editable="true" />  <img src="" pardot-region="pardot_image" pardot-region-type="image" />  <img src="" mc:edit="image_%s" />
*** <repeater>          <div>                <div mc:repeatable>                            <repeater>                      <div>                                                                   <div>
*** <unsubscribe>       <a target="_blank">  <a target="_blank" href="*|UNSUB|*">           <unsubscribe>                   <a target="_blank" href="%%unsubscribe%%">                              <a target="_blank">
*** <forwardtoafriend>  <a target="_blank">  <a target="_blank" href="*|FORWARD|*">         <forwardtoafriend>              <a target="_blank">                                                     <a target="_blank">
*** <webversion>        <a target="_blank">  <a target="_blank" href="*|ARCHIVE|*">         <webversion>                    <a target="_blank" href="%%view_online%%">                              <a target="_blank">
*** <preferences>       <a target="_blank">  <a target="_blank" href="*|UPDATE_PROFILE|*">  <preferences>                   <a target="_blank" href="%%email_preference_center%%">                  <a target="_blank">
*** <email>             <span>               *|EMAIL|*                                      [email]                         <span>%%email%%</span>                                                  <span>
*** <address>           <span>               *|LIST:ADDRESS|*                               <multiline>                     <span>%%account_address%%</span>                                        <span>
*** <companyname>       <span>               *|LIST:COMPANY|*                               <singleline>                    <span>%%company%%</span>'                                               <span>
*** <currentyear>       <span>               *|CURRENT_YEAR|*                               <currentyear>                   <span>                                                                  <span>
*** <currentmonth>      <span>               *|DATE:m|*                                     <currentmonth>                  <span>                                                                  <span>
*** <currentmonthname>  <span>               *|DATE:F|*                                     <currentmonthname>              <span>                                                                  <span>
*** <currentday>        <span>               *|DATE:d|*                                     <currentday>                    <span>                                                                  <span>
*** <currentdayname>    <span>               *|DATE:l|*                                     <currentdayname>                <span>                                                                  <span>
*** ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
**/


$tags = array(
    '|<multiline([^>]*)>|i' => array(
        'html'=>'<div$1>',
        'mailchimp'=>'<div$1 mc:edit="text_%s">',
        'hubspot'=>'{% widget_block rich_text "text_%s" label="Text_%v" %}{% widget_attribute "html" %}',
        'campaignmonitor'=>'<multiline$1>',
        'pardot'=>'<div$1 pardot-region="">',
        'autopilot'=>'<div$1 mc:edit="text_%s">',
        'marketo'=>'<div$1 class="mktEditable" id="text_%s">',
    ),
    
    '|</multiline>|' => array(
        'html'=>'</div>',
        'mailchimp'=>'</div>',
        'hubspot'=>'{% end_widget_attribute %}{% end_widget_block %}',
        'campaignmonitor'=>'</multiline>',
        'pardot'=>'</div>',
        'autopilot'=>'</div>',
        'marketo'=>'</div>',
    ),
    
    '|<singleline([^>]*)>|i' => array(
        'html'=>'<div$1>',
        'mailchimp'=>'<div$1 mc:edit="title_%s">',
        'campaignmonitor'=>'<singleline$1>',
        'pardot'=>'<div$1>',
        'autopilot'=>'<div$1>',
        'marketo'=>'<div$1 class="mktEditable" id="image_%s">',
    ),
    
    '|</singleline>|' => array(
        'html'=>'</div>',
        'mailchimp'=>'</div>',
        'campaignmonitor'=>'</singleline>',
        'pardot'=>'</div>',
        'autopilot'=>'</div>',
        'marketo'=>'</div>',
    ),
    
    '|<image(.*?)src="(.*?)" width="(.*?)" height="(.*?)"(.*?)/>|i' => array(
        'html'=>'<img$1src="$2" width="$3" height="$4"$5/>',
        'mailchimp'=>'<img$1src="$2" width="$3" height="$4" mc:edit="image_%s" style="max-width:$3px;"$5/>',
        'campaignmonitor'=>'<img$1src="$2" width="$3" height="$4" editable="true"$5/>',
        'hubspot'=>'{% linked_image "image_%s" label="image_%s", src="$2", width="$3", height="$4", style="max-width: 100% !important; max-height: 100% !important; border: 0px; display:inline;", link="#" %}',
        'pardot'=>'<img$1src="$2" width="$3" height="$4" pardot-region="pardot_image" pardot-region-type="image" alt="" />',
        'autopilot'=>'<img$1src="$2" width="$3" height="$4" mc:edit="image_%s"$5/>',
    ),
    
    '|<repeater([^>]*)>|i' => array(
        'html'=>'<div>',
        'mailchimp'=>'<div mc:repeatable>',        
        'campaignmonitor'=>'<repeater>',
        'hubspot'=>'{% boolean "section%s" $1, value="true", export_to_template_context=True %}{% if widget_data.section%n.value == true %}',
        'pardot'=>'<div>',
        'autopilot'=>'<div>',
    ),
       
    '|</repeater>|i' => array(
        'html'=>'</div>',
        'mailchimp'=>'</div>',
        'hubspot'=>'{% endif %}',
        'campaignmonitor'=>'</repeater>',
        'pardot'=>'</div>',
        'autopilot'=>'</div>',
    ),
    
    '|<unsubscribe([^>]*)>|i' => array(
        'html'=>'<a$1 target="_blank" href="#">',
        'mailchimp'=>'<a$1 target="_blank" href="*|UNSUB|*">',
        'campaignmonitor'=>'<unsubscribe$1>',
        'pardot'=>'<a$1 target="_blank" href="%%unsubscribe%%">',
        'autopilot'=>'<a$1 target="_blank" href="#">',
    ),
    
    '|</unsubscribe>|i' => array(
        'html'=>'</a>',
        'mailchimp'=>'</a>',
        'campaignmonitor'=>'</unsubscribe>',
        'pardot'=>'</a>',
        'autopilot'=>'</a>',
    ),
    
    '|<webversion([^>]*)>|i' => array(
        'html'=>'<a$1 target="_blank" href="#">',
        'mailchimp'=>'<a$1 target="_blank" href="*|ARCHIVE|*">',
        'campaignmonitor'=>'<webversion$1>',
        'pardot'=>'<a$1 target="_blank" href="%%view_online%%">',
        'autopilot'=>'<a$1 target="_blank" href="#">',
    ),
    
    '|</webversion>|i' => array(
        'html'=>'</a>',
        'mailchimp'=>'</a>',
        'campaignmonitor'=>'</webversion>',
        'pardot'=>'</a>',
        'autopilot'=>'</a>',
    ),
    
    '|<forwardtoafriend([^>]*)>|i' => array(
        'html'=>'<a$1 target="_blank" href="#">',
        'mailchimp'=>'<a$1 target="_blank" href="*|FORWARD|*">',
        'campaignmonitor'=>'<forwardtoafriend$1>',
        'pardot'=>'<a$1 target="_blank" href="#">',
        'autopilot'=>'<a$1 target="_blank" href="#">',
    ),
    
    '|</forwardtoafriend>|i' => array(
        'html'=>'</a>',
        'mailchimp'=>'</a>',
        'campaignmonitor'=>'</forwardtoafriend>',
        'pardot'=>'</a>',
        'autopilot'=>'</a>',
    ),
    
    '|<preferences([^>]*)>|i' => array(
        'html'=>'<a$1 target="_blank" href="#">',
        'mailchimp'=>'<a$1 target="_blank" href="*|UPDATE_PROFILE|*">',
        'campaignmonitor'=>'<preferences$1>',
        'pardot'=>'<a$1 target="_blank" href="%%email_preference_center%%">',
        'autopilot'=>'<a$1 target="_blank" href="#">',
    ),
    
    '|</preferences>|i' => array(
        'html'=>'</a>',
        'mailchimp'=>'</a>',
        'campaignmonitor'=>'</preferences>',
        'pardot'=>'</a>',
        'autopilot'=>'</a>',
    ),
    
    
    '|<email([^>]*)>(.*?)</email>|i' => array(
        'html'=>'<span$1>$2</span>',
        'mailchimp'=>'<span$1>*|EMAIL|*</span>',
        'campaignmonitor'=>'<span$1>[email]</span>',
        'pardot'=>'<span$1>%%email%%</span>',
        'autopilot'=>'<span$1>$2</span>',
    ),
    
    '|<address([^>]*)>(.*?)</address>|i' => array(
        'html'=>'<span$1>$2</span>',
        'mailchimp'=>'<span$1>*|LIST:ADDRESS|*</span>',
        'campaignmonitor'=>'<multiline$1>$2</multiline>',
        'pardot'=>'<span$1>%%account_address%%</span>',
        'autopilot'=>'<span$1>$2</span>',
    ),
    
    '|<companyname([^>]*)>(.*?)</companyname>|i' => array(
        'html'=>'<span$1>$2</span>',
        'mailchimp'=>'<span$1>*|LIST:COMPANY|*</span>',
        'campaignmonitor'=>'<singleline$1>$2</singleline>',
        'pardot'=>'<span$1>%%company%%</span>',
        'autopilot'=>'<span$1>$2</span>',
    ),
    
    # 2012
    '|<currentyear([^>]*)>(.*?)</currentyear>|i' => array(
        'html'=>'<span$1>$2</span>',
        'mailchimp'=>'<span$1>*|CURRENT_YEAR|*</span>',
        'campaignmonitor'=>'<currentyear$1>',
        'pardot'=>'<span$1>$2</span>',
        'autopilot'=>'<span$1>$2</span>',
    ),
    
    # 06
    '|<currentmonth([^>]*)>(.*?)</currentmonth>|i' => array(
        'html'=>'<span$1>$2</span>',
        'mailchimp'=>'<span$1>*|DATE:m|*</span>',
        'campaignmonitor'=>'<currentmonth$1>',
        'pardot'=>'<span$1>$2</span>',
        'autopilot'=>'<span$1>$2</span>',
    ),
    
    # June
    '|<currentmonthname([^>]*)>(.*?)</currentmonthname>|i' => array(
        'html'=>'<span$1>$2</span>',
        'mailchimp'=>'<span$1>*|DATE:F|*</span>',
        'campaignmonitor'=>'<currentmonthname$1>',
        'pardot'=>'<span$1>$2</span>',
        'autopilot'=>'<span$1>$2</span>',
    ),
    
    # 12
    '|<currentday([^>]*)>(.*?)</currentday>|i' => array(
        'html'=>'<span$1>$2</span>',
        'mailchimp'=>'<span$1>*|DATE:d|*</span>',
        'campaignmonitor'=>'<currentday$1>',
        'pardot'=>'<span$1>$2</span>',
        'autopilot'=>'<span$1>$2</span>',
    ),
    
    # Monday
    '|<currentdayname([^>]*)>(.*?)</currentdayname>|i' => array(
        'html'=>'<span$1>$2</span>',
        'mailchimp'=>'<span$1>*|DATE:l|*</span>',
        'campaignmonitor'=>'<currentdayname$1>',
        'pardot'=>'<span$1>$2</span>',
        'autopilot'=>'<span$1>$2</span>',
    ),
);

function parse_platform($platform, $html){
    global $tags, $_number, $_number2, $_number3, $_number4;
    
    $out = $html;
    foreach($tags as $pattern=>$replacements) {
        $replacement = $replacements[$platform];
        $_number = 1;
        $_number2 = 1;
        $_number3 = 1;
        $_number4 = 1;
        $out = preg_replace($pattern, $replacement, $out);
        $out = preg_replace_callback('|%s|', 'replace_number',$out);
        $out = preg_replace_callback('|%v|', 'replace_number2',$out);
        $out = preg_replace_callback('|%p|', 'replace_number3',$out);
        $out = preg_replace_callback('|%n|', 'replace_number4',$out);
    }
    return $out;
}

function replace_number($matches){
    global $_number;
    return $_number++;
}

function replace_number2($matches){
    global $_number2;
    return $_number2++;
}

function replace_number3($matches){
    global $_number3;
    return $_number3++;
}
function replace_number4($matches){
    global $_number4;
    return $_number4++;
}

?>