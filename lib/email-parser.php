<?php
# Template Tags
include_once('email-template-tags.php');
include_once('helpers.php');

# Parse email html code
function parse_email_template($template){
    global $css_code; // this will be used for all css rules
    global $css_styles; // same as above but array

    # filename - without the extension
    $filename = explode('/', $_SERVER['SCRIPT_FILENAME']); $filename = $filename[count($filename) - 1];
    $folder = str_replace($filename, '', $_SERVER['SCRIPT_FILENAME']);
    
    # check if this is link to file or the actual file contents string
    if( file_exists($template) ) {
        $path = $template;
        $path_args = explode('/', $path);

        $filename = $path_args[count($path_args) - 1];
        $folder = str_replace($filename, '', $path);

        $template = file_get_contents($template);
    }

    $clean_filename = str_replace(array('.php', 'html'), '', $filename);

    # patterns
    $link_pattern = '|<link([^>]*)rel="stylesheet"([^>]*)href="(.*?)"([^>]*)/>|i';  
    $inline_style_pattern = '|<style type="text/css">([^>]*)</style>|i';
    $screen_style_pattern = '|<style type="text/css" media="linked">([^>]*)</style>|i';
    $br_pattern = '|<br height="([^"]*)"([^>]*type="([^"]*)")?([^>]*)>|i';
    $mobilebr_pattern = '|<mobilebr height="([^>]*)"([^>]*)/>|i';
    $border_pattern = '|<border height="([^"]*)"([^>]*)color="([^"]*)"([^>]*type="([^"]*)")?([^>]*)>|i';

    # get the external css files content
    preg_match_all($link_pattern, $template, $matches);

    if( isset($matches[3]) ) {
        foreach($matches[3] as $css_file) {
            $css_contents = @file_get_contents($folder . $css_file);
            if( $css_contents ) {
                $css_code .= '/* ' . $css_file .  ' */';
                $css_code .= "\n" . $css_contents;
                $css_code .= "\n\n";
            }
        }    
    }
    
    # get the inline styles
    preg_match_all($inline_style_pattern, $template, $matches);

    if( isset($matches[1]) ) {
        $i = 1;
        foreach($matches[1] as $css_contents) {
            $css_code .= '/* head style ' . $i .  ' */';
            $css_code .= "\n" . $css_contents;
            $css_code .= "\n\n";
            $i++;
        }
    }

    # transform css code to array
    $css_styles = get_styles( $css_code );

    # .background-something type definitions
    $css_backgrounds = array();
    $transformed_backgrounds = array();
    foreach($css_styles as $selector=>$rule) {
        $pos = strpos($selector, 'background-');
        if( $pos !== false && $pos == 0 ) {
            $css_backgrounds[$selector] = $rule;
            $transformed_backgrounds['.' . $selector] = str_replace('background:', '', $rule);
        }
    }
    
    $background_rules = array('background:$;', 'bgcolor="$"', 'color="$"', 'background="$"');

    $background_replacements = array();
    foreach( $transformed_backgrounds as $selector=>$value ) {
        foreach( $background_rules as $rule ) {
            $k = str_replace('$', $selector, $rule);
            $v = str_replace('$', $value, $rule);
            $background_replacements[ $k ] = $v;
        }
    }

    #trace($background_replacements, 1);

    # keep the original template
    $html_code = $template;

    # replace brs - <br height="10" />
    $html_code = preg_replace_callback($br_pattern, 'replace_br', $html_code);

    # replace brs - <br height="10" />
    $html_code = preg_replace_callback($mobilebr_pattern, 'replace_mobile_br', $html_code);

    # replace border - <border height="20" color="#dddddd" /> with image based border
    $html_code = preg_replace_callback($border_pattern, 'replace_border', $html_code);

    # replace __path__
    $html_code = str_replace('__path__', p(), $html_code);

    # copy and keep html_code, since the below modifications are only for inline-css-html-code
    $parsed_code = $html_code;

    # remove all the style tags - <style> and <link>;
    $parsed_code = preg_replace($link_pattern, '', $html_code);
    $parsed_code = preg_replace($inline_style_pattern, '', $parsed_code);

    # parse the CSS style as inline style css
    # moved inside nested foreach

    # linked styles replace a.test { .test }
    $screen_css = preg_match($screen_style_pattern, $parsed_code, $matches);
    if( isset($matches[1]) ) {
        $screen_css = $matches[1];
        $new_screen_css = preg_replace_callback('|(.*?)\{(.*?)\}|i', 'replace_css_values', $screen_css);
        $parsed_code = preg_replace($screen_style_pattern, '<style type="text/css" media="screen">' . $new_screen_css . '</style>', $parsed_code);    
    }

    # replaces backgrounds
    # moved inside nested foreach

    # replace media="linked" with
    $html_code = str_replace('<style type="text/css" media="linked">', '<style type="text/css" media="screen">', $html_code);

    # 
    $parsed_code = replace_inline_style_declarations( $parsed_code );
    $html_code = replace_inline_style_declarations( $html_code );

    # all templates are stored here
    $templates = array();

    # proccess the default, mailchimp, campaignmonitor tags
    foreach( get_platforms() as $platform ) {
        foreach( get_css_types() as $css_type ) {
            # regular templates do not need parsed inline-css-code
            $code_to_use = $css_type == 'default' ? $parsed_code : $html_code;

            if ( should_parse_template_language() ) {
                $platform_parsed_code = parse_platform($platform, $code_to_use);
                if ( $css_type !== 'regular' ) {
                    $platform_parsed_code = replace_classes( $platform_parsed_code );
                }
                $platform_parsed_code = str_replace(array_keys($background_replacements), array_values($background_replacements), $platform_parsed_code);
                $templates[$css_type][$platform] = $platform_parsed_code;
            } else {
                if ( $css_type !== 'regular' ) {
                    $code_to_use = replace_classes( $code_to_use );
                }
                $code_to_use = str_replace(array_keys($background_replacements), array_values($background_replacements), $code_to_use);
                $templates[$css_type][$platform] = $code_to_use;
            }
            
        }
    }

    #trace($templates);
    # first check if we should save the templates as html files
    if( should_save_html() ) {

        # save the templates as files    
        foreach( $templates as $css_type => $tpls ) {
            foreach( $tpls as $platform=>$html ) {
                
                $sub_folder = '';
                if( $css_type != 'default' ) {
                    $sub_folder = 'templates-' . $css_type . '/';
                }

                $new_filename = $clean_filename;
                if( $platform != 'html' ) {
                    $new_filename .= '-' . $platform;
                }
                $new_filename .= '.html';

                if( !file_exists($folder.$sub_folder) ) {
                    mkdir($folder.$sub_folder);
                }

                $template_filename = $folder . $sub_folder . $new_filename;

                file_put_contents($template_filename, $html);
            }
        }
    }
    
    

    # returns the default code
    $return_key = get_return_keys();
    
    # return if are set the default keys
    if( isset($templates[ $return_key[0] ][ $return_key[1] ]) ) {
        return $templates[ $return_key[0] ][ $return_key[1] ];
    }

    # if not returns the first template
    $defaut_type = get_css_types(); $defaut_type = $defaut_type[0];
    $default_platform = get_platforms(); $default_platform = $default_platform[0];

    return $templates[ $defaut_type ][ $default_platform ];
}

# spacer function
function br( $height, $type ) {
    $type = $type ? $type : 'table';

    if ($type === 'div') {
        $img = '<img src="__path__/spacer.gif" width="1" height="'.$height.'" style="height:'.$height.'px" alt="" />';
        return '<div style="font-size:0pt; line-height:0pt; height:'.$height.'px">'.$img.'</div>'."\n";
    }

    return '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer"><tr><td height="'.$height.'" class="spacer">&nbsp;</td></tr></table>'."\n";
}

function mobile_br( $height ) {
    $img = '<img src="__path__/spacer.gif" width="1" height="0" alt="" />';
    return '<div style="font-size:0pt; line-height:0pt;" class="m-br-' . $height . '"></div>'."\n";
}

# border function
function border( $height, $color, $type ) {
    $type = $type ? $type : 'table';

    if ($type === 'div') {
        $img = '<img src="__path__/spacer.gif" width="1" height="'.$height.'" style="height:'.$height.'px" alt="" />';
        return '<div style="font-size:0pt; line-height:0pt; height:'.$height.'px; background:'.$color.'; ">'.$img.'</div>' . "\n";
    }

    return '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="'.$color.'" class="border"><tr><td bgcolor="'.$color.'" height="'.$height.'" class="border">&nbsp;</td></tr></table>'."\n";
}

# get all styles and covert them to array
function get_styles( $css ) {
    $css = preg_replace('~\r\n~', '', $css);
    $css = preg_replace('~\t~', ' ', $css);
    $css = preg_replace('|\;(\s*?)\}|i', '}', $css); // removing the last ;
    preg_match_all('|\.(.*?)\{(.*?)\}|i', $css, $matches); // key + value

    $css_properties = array();
    $css_styles = array();
    
    $class_names = $matches[1];
    $values = $matches[2];

    $i=0;
    foreach($class_names as $class_name) {
        $v = trim($values[$i]);
        if(!empty($v)){
            $css_properties[trim(strtolower($class_name))][] = $v;
        }
        $i++;
    }

    foreach( $css_properties as $class_name=>$css_rules ) {
        $css_styles[$class_name] = count($css_rules) > 1 ? implode('; ', $css_rules) : $css_rules[0];
    }

    # remove duplicated values - the last is the most important
    foreach( $css_styles as $class_name=>$css_declaration ) {
        $rules = explode(';', $css_declaration);

        $new_rules = array();
        foreach($rules as $rule) {
            $e = explode(':', $rule, 2);
            $key = trim($e[0]);
            $value = trim($e[1]);
            if( isset($new_rules[$key]) ) {
                unset($new_rules[$key]);
            }
            $new_rules[$key] = $value;
        }

        $update_rules = array();
        foreach($new_rules as $k=>$v) {
            $update_rules[] = $k . ':' . $v;
        }

        $css_styles[$class_name] = implode('; ', $update_rules);
    }

    $css_classes = array_keys( $css_styles );
    $css_classes = array_map(function ($class) {
        return '.' . $class;
    }, $css_classes);

    foreach ( $css_styles as &$style ) {
        // we need all selector properties
        // in order to check if there are class declarations that need replacing
        $properties = explode( ';', $style );
        // if none are present, its probably only one declaration
        if ( empty( $properties ) ) {
            $properties = array( $style );
        }

        foreach ( $properties as $property ) {
            $property_parts = explode( ':', $property );
            // in case it's an invalid property
            if ( empty( $property_parts ) ) {
                continue;
            }

            // go through each individual property to see if it has a class declaration that needs replacing
            foreach ( $property_parts as $key => $value ) {
                $value = trim( $value );
                // if none - skip
                if ( ! in_array( $value, $css_classes ) ) {
                    continue;
                }

                // if found, get properties of that class and replace the actual property declaration with
                $class_name = ltrim( $value, '.' );
                if ( array_key_exists( $class_name, $css_styles ) ) {
                    $style = str_replace( trim( $property ), $css_styles[ $class_name ], $style );
                }
            }
        }
    }

    return $css_styles;
}

function replace_inline_style_declarations( $content ) {
    global $css_styles;

    $css_classes = array_keys( $css_styles );
    $css_classes = array_map(function ($class) {
        return '.' . $class;
    }, $css_classes);
    $css_properties = array_values( $css_styles );

    $inline_style_pattern = '|<style type="text/css".*>([^>]*)</style>|i';

    preg_match_all( $inline_style_pattern, $content, $style_declarations );
    foreach ( $style_declarations as $inline_styles ) {
        foreach ( $inline_styles as $inline_style ) {
            // get only css class declarations - skip comments and empty rows
            $declarations = array_filter( array_map( function ( $row ) {
                return strpos( $row, '{' ) !== false ? trim( $row ) : false;
            }, preg_split( '~\R~', $inline_style ) ) );
            foreach ( $declarations as $declaration ) {
                // we need all selector properties
                // in order to check if there are class declarations that need replacing
                preg_match( '~{(.*)}~', $declaration , $matches );
                if ( ! isset( $matches[1] ) ) {
                    continue;
                }

                $sanitized_declaration = trim( $matches[1] );
                $properties = explode( ';', $sanitized_declaration );
                // if none are present, its probably only one declaration
                if ( empty( $properties ) ) {
                    $properties = array( $sanitized_declaration );
                }

                foreach ( $properties as $property ) {
                    if ( strpos( $property, ':' ) === false ) {
                        $class_name = ltrim( $property, '.' );
                        if ( array_key_exists( $class_name, $css_styles ) ) {
                            $new_declaration = str_replace( trim( $property ), $css_styles[ $class_name ], $declaration );
                            $content = str_replace( $declaration, $new_declaration, $content );
                        }
                        continue;
                    }

                    $property_parts = explode( ':', $property );
                    if ( empty( $property_parts ) ) {
                        continue;
                    }

                    // go through each individual property to see if it has a class declaration that needs replacing
                    foreach ( $property_parts as $key => $value ) {
                        $value = trim( $value );
                        // if none - skip
                        if ( ! in_array( $value, $css_classes ) ) {
                            continue;
                        }

                        // if found, get properties of that class and replace the actual property declaration with
                        $class_name = ltrim( $value, '.' );
                        if ( array_key_exists( $class_name, $css_styles ) ) {
                            $new_declaration = str_replace( trim( $property ), $css_styles[ $class_name ], $declaration );
                            $content = str_replace( $declaration, $new_declaration, $content );
                        }
                    }
                }
            }
        }
    }

    return $content;
}

function implode_properties( $declarations ) {
    return implode('; ', $declarations);
}

function replace_classes( $content ) {
    global $css_styles;

    preg_match_all( '~<[^<]*class="[^>]*>~', $content, $content_tags );
    if ( empty( array_filter( $content_tags ) ) ) {
        return $content;
    }

    $tags = array_shift( $content_tags );

    $elements_with_classes = [];
    foreach ( $tags as $element ) {
        preg_match_all( '~class="([^"]*)"~', $element, $element_classes );
        if ( empty( array_filter( $element_classes ) ) ) {
            continue;
        }

        if ( empty( $element_classes[1] ) ) {
            continue;
        }

        $existing_classes = array_shift( $element_classes[1] );
        if ( ! $existing_classes ) {
            continue;
        }

        $classes = explode( ' ', $existing_classes );

        $new_styles = [];

        preg_match_all( '~style="([^"]*)"~', $element, $existing_tag_styles );
        if ( !empty( array_filter( $existing_tag_styles ) ) && !empty( $existing_tag_styles[1] ) ) {
            $new_styles[] = array_shift( $existing_tag_styles[1] );
        }

        foreach ( $classes as $class_name ) {
            if ( ! isset( $css_styles[ $class_name ] ) ) {
                continue;
            }

            $style = $css_styles[ $class_name ];
            $new_styles[] = strpos( $style, ';' ) !== strlen( $style ) ? $style . ';' : $style;
        }

        if ( !empty( $new_styles ) ) {
            $new_styles = cleanup_styles( $new_styles );
            if ( strpos( $element, 'style="' ) === false ) {
                preg_match( '~\/\s*?>$~', $element, $self_closing_tag_matches );
                if ( !empty( $self_closing_tag_matches ) ) {
                    $new_element = preg_replace( '~\/\s*?>$~', ' style="' . $new_styles . '" />', $element );
                } else {
                    $new_element = preg_replace( '~>$~', ' style="' . $new_styles . '">', $element );
                }
            } else {
                $new_element = preg_replace( '~style="[^"]*"~', 'style="' . $new_styles . '"', $element );
            }
            $content = str_replace( $element, $new_element, $content );
        }
    }

    return $content;
}

# path
function p() {
    global $p;
    return $p;
}

# replace css
function replace_css_values() {
    global $css_styles;

    $matches = func_get_args();
    $style_class_name = trim(str_replace('.', '', $matches[0][2]));
    $css_class_name = $matches[0][1];

    if( isset( $css_styles[$style_class_name] ) ) {
        return $css_class_name . '{ ' . $css_styles[$style_class_name] . ' }';
    }
    return $css_class_name . '{ ' . $style_class_name . ' }';
    
}

# br tags
function replace_br() {
    $matches = func_get_args();
    return br($matches[0][1], $matches[0][3]);
}

function replace_mobile_br() {
    $matches = func_get_args();
    return mobile_br($matches[0][1]);
}

# custom border
function replace_border() {
    $matches = func_get_args();
    return border($matches[0][1], $matches[0][3], $matches[0][5]);
}

# platforms
function get_platforms() {
    $plaforms = 'html';
    if( defined('PLATFORMS') ) {
        $plaforms = PLATFORMS;
    }
    return explode(',', $plaforms);
}

function is_platform( $platform ) {
    return in_array($platform, get_platforms());
}


# css types
function get_css_types() {
    $types = 'default';
    if( defined('CSS_TYPE') ) {
        $types = CSS_TYPE;
    }
    return explode(',', $types);
}

function is_css_type( $css ) {
    return in_array($css, get_css_types());
}

function get_return_keys() {
    $r = 'default-html';
    if( defined('RETURN_TEMPLATE') ) {
        $r = RETURN_TEMPLATE;
    }
    return explode('-', $r);
}

function should_save_html() {
    if( !defined('SAVE_HTML') ) {
        return false;
    }
    if( SAVE_HTML ) {
        return true;
    }
    return false;
}

function should_parse_template_language() {
    if( defined('PARSE_TEMPLATE_LANGUAGE') && PARSE_TEMPLATE_LANGUAGE == true ) {
        return true;
    }
    
    $platforms = get_platforms();
    if( count($platforms) == 1 && $platforms[0] == 'html' ) {
        return false;
    }else {
        return true;
    }
    return false;
}

# trace - debug func
# compatible with other libraries
if( !function_exists('trace') ) {
    function trace($input, $exit=false) {
        echo '<fieldset class="debug" style="background: #fff; text-align: left; padding: 15px; margin: 15px; border: solid 1px red">';
        echo '<legend style="color: red; padding: 4px 15px; background: #fff; border: solid 1px red;">Debugging <b>' . (is_object($input) ? ' object ' . get_class($input) : gettype($input))  . '</b></legend>';   
        echo '<pre>';
        if(is_bool($input)) {
            echo '<b>';
            var_dump($input);
            echo '</b>';
        } elseif(is_scalar($input)) {
            echo '<b>' . $input . '</b>';
        } else {
            print_r($input);
        }
        echo '</pre>';
        echo '</fieldset>';
        if($exit) exit('Script execution stopped.');
    }
}
?>