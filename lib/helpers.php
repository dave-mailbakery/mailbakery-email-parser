<?php

// takes an array with styles, from elements with multiple classes
// should remove duplicate style declarations, where the last declaration has the highest priority
// example:
// .c1 { font-size: 22px; line-height: 24px; color: #eee; }
// .c2 { font-size: 10px; line-height: 12px; }
// <body class="c1 c2">
// should convert to
// <body class="c1 c2" style="color: #eee; font-size: 10px; line-height: 12px;">
function cleanup_styles( $styles ) {
    $sanitized_styles = array();

    $unique_css_properties = array();

    // reverse the array, so we can get the unique declarations first
    $reversed_styles = array_reverse( $styles );
    foreach ( $reversed_styles as $style ) {
        $style_parts = array_filter( explode( ';', $style ) );

        $temp_styles = array_filter( array_map( function ( $declaration ) use ( &$unique_css_properties ) {
            $declaration = trim( $declaration );
            $property = explode( ':', $declaration );
            $property_key = strtolower( $property[0] );
            if ( ! in_array( $property_key, $unique_css_properties ) ) {
                $unique_css_properties[] = $property_key;
                return $declaration;
            }
        }, $style_parts ) );

        $sanitized_styles = array_merge( $temp_styles, $sanitized_styles );
    }
    $sanitized_styles = implode( '; ', $sanitized_styles ) . ';';

    return $sanitized_styles;
}
